# Instruction

1. Fork this repository and clone it to your desktop or labtop.
2. **If you're using GitHub Desktop**, import this repository to your eclipse's workspace.
3. Add the external jar files inside folder `required jar` to the project's build path 
4. Read the problem `2110215_Lab3_2016.pdf` posted on CourseVille and the worksheet in this repository thoroughly.
5. Implement all classes following the detail in the problem. Also finish the worksheet.
  * Note that all classes you implement are inside package `logic`.
6. Generate UML Class diagram using ObjecAid in Eclipse.
  * Your UML Class diagram must consists of all the classes inside package `logic`
  * Save both ObjectAid ucls `uml.ucls` file and image as `uml.png` in project root directory
7. Export your finished project into a runnable jar file named `Lab3_2016_{ID}.jar` where ID is your Student ID.
  * For example, `Lab3_2016_5770257521.jar`.
  * Make sure to export it into your project root directory.
8. Commit and push it to your GitHub repository.
9. Send a Pull Request to us with Title: `[STUDENT-ID] Submission`.

# Deadline
Thursday 6th October 2016
