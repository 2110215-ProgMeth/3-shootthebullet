package logic;

import lib.DrawingUtility;

public class ItemBullet extends CollectibleObject{

	public ItemBullet(int movingDuration, int z) {
		super(50,movingDuration,z,30);
	}

	@Override
	public void render() {
		DrawingUtility.drawItemBullet(x, y, radius, isPointerOver);
	}

	@Override
	public void collect(PlayerStatus player) {
		if(player.getCurrentGun() instanceof SpecialGun){
			SpecialGun spg=((SpecialGun)player.getCurrentGun());
			spg.setBulletQuantity(Integer.MAX_VALUE);
		}
	}

}
