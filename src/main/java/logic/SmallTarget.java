package logic;

import lib.ConfigurableOption;
import lib.DrawingUtility;
import lib.RandomUtility;

public class SmallTarget extends ShootableObject {

	public SmallTarget(int radius, int movingDuration, int z){
		super(radius,movingDuration,z,5);
		setLife(2);
	}
	
	public SmallTarget(int radius, int movingDuration, int z, 
			int startX,int startY) {
		this(radius, movingDuration, z);
				
		movingType = 0;
		if(RandomUtility.random(0, 1) == 1){
			movingParameter = new int[]{
					startX,startY,
					RandomUtility.random(0,1) == 0 ? -radius : ConfigurableOption.screenWidth+radius,
					RandomUtility.random(-radius, ConfigurableOption.screenHeight+radius)
			};
		}else{
			movingParameter = new int[]{
					startX,startY,
					RandomUtility.random(-radius, ConfigurableOption.screenWidth+radius),
					RandomUtility.random(0,1) == 0 ? -radius : ConfigurableOption.screenHeight+radius
			};
		}
	}

	@Override
	public void render() {
		DrawingUtility.drawShootableObject(x, y, radius, "small", isPointerOver);		
	}

}
