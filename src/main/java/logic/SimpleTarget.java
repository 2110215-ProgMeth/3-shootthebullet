package logic;


import lib.DrawingUtility;

public class SimpleTarget extends ShootableObject {

	public SimpleTarget(int radius,int movingDuration,int z) {
		super(radius,movingDuration,z,3);
		setLife(3);
	}

	@Override
	public void render() {
		DrawingUtility.drawShootableObject( x, y, radius, "simple", isPointerOver);
	}

}
