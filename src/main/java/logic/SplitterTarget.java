package logic;

import java.util.List;
import lib.DrawingUtility;
import lib.RandomUtility;
public class SplitterTarget extends ShootableObject{

	private List<TargetObject> onScreenObject;
	
	public SplitterTarget(int radius, int movingDuration, int z,
			List<TargetObject> onScreenObject) {
		super(radius,movingDuration,z,5);
		setLife(5);
		this.onScreenObject = onScreenObject;
	}
	
	@Override
	public void hit(PlayerStatus player){
		super.hit(player);
		if(isDestroyed){
			int n=RandomUtility.random(3, 6);
			for(int i=0;i<n;i++){
				onScreenObject.add(new SmallTarget(radius/2, movingDuration, z,x,y));
			}
		}
	}
	
	@Override
	public void render() {
		DrawingUtility.drawShootableObject(x, y, radius, "splitter", isPointerOver);
	}

}
