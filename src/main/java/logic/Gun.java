package logic;

import lib.AudioUtility;
import lib.DrawingUtility;
import lib.IRenderableObject;

public class Gun implements IRenderableObject {

	protected int attack;
	
	public Gun(int attack) {
		this.attack=attack;
	}

	public int getAttack() {
		return attack;
	}

	public boolean canShoot(){
		return true;
	}
	
	public void shoot(){
		AudioUtility.playSound("shoot");
	}

	@Override
	public int getZ() {
		return 0;
	}

	@Override
	public boolean isVisible() {
		return false;
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		//								*ANY*				*ANY*
		DrawingUtility.drawIconGun(Integer.MAX_VALUE, Integer.MAX_VALUE, true);
	}
	
}
