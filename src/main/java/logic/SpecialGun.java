package logic;

import lib.DrawingUtility;

public class SpecialGun extends Gun {

	protected int bulletQuantity;
	protected int maxBullet;
	
	public SpecialGun(int bulletQuantity, int maxBullet, int attack) {
		super(attack);
		this.maxBullet=maxBullet;
		setBulletQuantity(bulletQuantity);
	}
	
	public int getBulletQuantity() {
		return bulletQuantity;
	}

	public void setBulletQuantity(int bulletQuantity) {
		this.bulletQuantity = Math.max(Math.min(bulletQuantity,maxBullet),0);
	}
	
	@Override
	public boolean canShoot(){
		return bulletQuantity>0;
	}
	@Override
	public void shoot(){
		super.shoot();
		bulletQuantity--;
	}
	
	@Override
	public void render() {
		DrawingUtility.drawIconGun(bulletQuantity, maxBullet, false);
	}
}
