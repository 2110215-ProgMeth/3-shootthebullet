package logic;

public abstract class ShootableObject extends TargetObject{

	protected int reward;
	protected int life;
	
	public ShootableObject(int radius, int movingDuration,int z,int reward) {
		super(radius,movingDuration,z);
		this.reward=reward;
	}
	
	public void setLife(int life){
		this.life=Math.max(0,life);
		if(this.life==0)isDestroyed=true;
	}
	
	public void hit(PlayerStatus player){
		setLife(life-player.getCurrentGun().getAttack());
		if(isDestroyed){
			player.increaseScore(reward);
		}
	}
}
