package logic;

import lib.DrawingUtility;

public class ItemSpecialGun extends CollectibleObject {
	
	public ItemSpecialGun(int movingDuration, int z) {
		super(50,movingDuration,z,50);
	}

	@Override
	public void render() {
		DrawingUtility.drawItemGun(x, y, radius, "gun", isPointerOver);
	}

	@Override
	public void collect(PlayerStatus player) {
		player.setCurrentGun(new SpecialGun(20, 20, 3));
	}

}
